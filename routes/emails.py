import logging
import webapp2
from ..app.workers import emailer
from google.appengine.api import taskqueue
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
import requests
import datetime


class AllMailMethod(InboundMailHandler):
    def receive(self, mail_message):

        logging.info("Received a message from: " + mail_message.sender)

        # Normally do some processing before kicking off a task
            # Do processing on the sender address (authentication)
                # Then do processing on the Subject
                # Then on the body looking for params - etc
                # Echo back the params with an attachement

        # For now just echo back the subject

        subject = 'RE:' + mail_message.subject
        body = 'I chose not to send your whole email back - just wanted to acknowledge that you sent one.'
        cc = ''

        if hasattr(mail_message, 'cc'):
        	cc = mail_message.cc

        taskqueue.add(url='/worker/emailer',
        			  params={'sender': mail_message.to, #echo back the variable inbox
        			  		  'to':mail_message.sender,  #send back to the original sender
        			  		  'cc':cc,                   #reply w/ cc if present
                              'subject': subject, 		 #pre-canned subject
                              'body':body})			     #pre-canned body


handlers = webapp2.WSGIApplication([AllMailMethod.mapping()], debug=True)

# example -
# test@koupon-media.appspotmail.com


# sender: The email address of the sender, the From address. The sender address must be one of the following types:
# The address of a registered administrator for the application. You can add administrators to an application using the Administration Console.
# The address of the user for the current request signed in with a Google Account. You can determine the current user's email address with the Users API. The user's account must be a Gmail account, or be on a domain managed by Google Apps.
# Any valid email receiving address for the app (such as xxx@APP-ID.appspotmail.com).

# to : A recipient's email address (a string) or a list of email addresses to appear on the To: line in the message header.

# cc : A recipient's email address (a string) or a list of email addresses to appear on the Cc: line in the message header.

# bcc : A recipient's email address (a string) or a list of email addresses to receive the message, but not appear in the message header ("blind carbon copy").

# reply_to : An email address to which a recipient should reply instead of the sender address, the Reply-To: field.

# subject : The subject of the message, the Subject: line.

# body : The plaintext body content of the message.

# html : An HTML version of the body content, for recipients that prefer HTML email.

# attachments : The file attachments for the message, as a list of two-value tuples, one tuple for each attachment. Each tuple contains a filename as the first element, and the file contents as the second element.
# An attachment file must be one of the allowed file types, and the filename must end with an extension that corresponds with the type. For a list of allowed types and filename extensions, see Overview: Attachments.

# headers :The headers for the message, as a dictionary. The keys are the header names, and the corresponding values are the header values.
# A header name must be one of the allowed headers. For a list of allowed header names, see the Mail Service Overview.