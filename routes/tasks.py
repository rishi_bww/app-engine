import logging
import webapp2

routes = webapp2.WSGIApplication([
    ('/tasks/weeklys', 'app.worker.dispatch.WeeklyDispatcher'),
    ('/tasks/monthlys', 'app.worker.dispatch.WeeklyDispatcher'),
    ('/tasks/report','app.workers.dispatch.SingleReport')

    # will need to be be changed to controllers that kick off a task queue
    # attempting to stop app breakage via poor URL routing
], debug=True)
