#-------------------------------------------------------------------------------
# Name:        ExcelFileGen
# Purpose:
#
# Author:      Rishabh Jain
#
# Created:     2013/06/18
# Copyright:   (c) Rishi 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from lib import pyExcelerator
import webapp2

class ExcelGen(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja2_environment.get_template('excelbook.html')
        self.response.out.write(template.render(
                                                url=url,
                                                url_linktext=url_linktext))
        wb = Workbook()
        ws0 = wb.add_sheet('Sheet 1')
        # Rows and columns count starts from 0
        for x in range(10):
            for y in range(10):
                # writing to a specific x,y
                ws0.write(x, y, "this is cell %s, %s" % (x,y))
        print ws0

        # HTTP headers to force file download
        self.response.headers['Content-Type'] = 'application/ms-excel'
        self.response.headers['Content-Transfer-Encoding'] = 'Binary'
        self.response.headers['Content-disposition'] = 'attachment; filename="workbook.xls"'

        # output to user
        wb.save(self.response.out)