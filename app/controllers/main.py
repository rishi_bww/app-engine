import jinja2
import os
import webapp2
import logging

from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api.mail import EmailMessage
from ..models.greeting import Greeting as Geeting
from ..models.TabList import TabsStatic as TabsStatic
from ..models.TabList import TabsDynamic as TabsDynamic

def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

class MainPage(webapp2.RequestHandler):
    def get(self):
        greetings_query = Geeting.query(ancestor=guestbook_key()).order(-Geeting.date)
        greetings = greetings_query.fetch(10)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja_environment.get_template('index.html')
        self.response.out.write(template.render(greetings=greetings,
                                                url=url,
                                                url_linktext=url_linktext))

class Guestbook(webapp2.RequestHandler):
    def post(self):
        greeting = Geeting(parent=guestbook_key())

        if users.get_current_user():
            greeting.author = users.get_current_user()

        greeting.content = self.request.get('content')
        greeting.put()

        self.redirect('/')

class ExcelGen(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja_environment.get_template('excelbook.html')
        self.response.out.write(template.render(
                                                url=url,
                                                url_linktext=url_linktext))
        wb = Workbook()
        ws0 = wb.add_sheet('Sheet 1')
        # Rows and columns count starts from 0
        for x in range(10):
            for y in range(10):
                # writing to a specific x,y
                ws0.write(x, y, "this is cell %s, %s" % (x,y))
        print ws0

        # HTTP headers to force file download
        self.response.headers['Content-Type'] = 'application/ms-excel'
        self.response.headers['Content-Transfer-Encoding'] = 'Binary'
        self.response.headers['Content-disposition'] = 'attachment; filename="workbook.xls"'

        # output to user
        wb.save(self.response.out)


class Addtab(webapp2.RequestHandler):
    def get(self):
            if users.get_current_user():
                url = users.create_logout_url(self.request.uri)
                url_linktext = 'Logout'
            else:
                url = users.create_login_url(self.request.uri)
                url_linktext = 'Login'

            template = jinja_environment.get_template('tabs_pg.html')
            self.response.out.write(template.render(
                                                    url=url,
                                                    url_linktext=url_linktext))
    def post(self):
        TabsStatic_db = TabsStatic()
        TabsStatic_db.TabName = self.request.get("TabName")
        TabsStatic_db.TabId = self.request.get("TabId")
        TabsStatic_db.Key = self.request.get("Key")
        TabsStatic_db.KeyValue = self.request.get("KeyValue")
        TabsStatic_db.put()

        TabsDynamic_db = TabsDynamic()
        TabsDynamic_db.TabName = self.request.get("TabName")
        TabsDynamic_db.TabId = self.request.get("TabId")
        TabsDynamic_db.ParamType = self.request.get("ParamType")
        TabsDynamic_db.ExtAcc = self.request.get("ExtAcct")
        TabsDynamic_db.ExtUserInput = self.request.get("ExtUserInput")
        TabsDynamic_db.put()


class DisplayTab(webapp2.RequestHandler):
    def get(self):
            TabsStatic_query = TabsStatic.query()
            TabsDynamic_query = TabsDynamic.query()
            #TabsStatic_data = TabsStatic_query.fetch(3)
            Tabs = TabsStatic.query(projection=('TabId', 'TabName'), distinct=True).fetch()

            if users.get_current_user():
                url = users.create_logout_url(self.request.uri)
                url_linktext = 'Logout'
            else:
                url = users.create_login_url(self.request.uri)
                url_linktext = 'Login'

            template = jinja_environment.get_template('DisplayTab.html')
            self.response.out.write(template.render(TabsStatic_query=TabsStatic_query,
                                                    TabsDynamic_query=TabsDynamic_query,
                                                    Tabs =Tabs,
                                                    url=url,
                                                    url_linktext=url_linktext))
