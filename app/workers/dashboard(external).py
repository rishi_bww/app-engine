#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     25/06/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import jinja2
import os
import webapp2
import logging

from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api.mail import EmailMessage
from ..models.ReportList import ReportList as List

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)

class Reporter(webapp2.RedirectHandler):
    def get(self):
        list = List.query(ancestor = guestbook_key())
        listreport = list.fetch(20)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja_environment.get_template('reporting.html')
        self.response.out.write(template.render(listreport=listreport,
                                                url=url,
                                                url_linktext=url_linktext))
