import logging
import webapp2

from google.appengine.api.mail import EmailMessage

class SendEmail(webapp2.RequestHandler):
    def post(self): # should run at most 1/s

        msg = EmailMessage(sender=self.request.get('sender'),
                           to=self.request.get('to'),
                           subject = self.request.get('subject'),
                           body = self.request.get('body'),
                           attachments = (self.request.get('attachment'),
                           'application/vnd.ms-excel'))
        if self.request.get('cc') != '':
            msg.initialize(cc=self.request.get('cc'))

        msg.send()

#        def txn():
#            counter = Counter.get_by_key_name(key)
#            if counter is None:
#                counter = Counter(key_name=key, count=1)
#            else:
#                counter.count += 1
#            counter.put()
#        db.run_in_transaction(txn)
