import jinja2
import os
import webapp2
import logging
from google.appengine.api.mail import EmailMessage
from google.appengine.ext import ndb
from google.appengine.api import users


from ..models.reports import Report

def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])


class MakeReport(webapp2.RequestHandler):
    def post(self): # should run at most 1/s

        ## Take in the query params
          ## Client ID, Start, Stop
          ## Tab Array
          ## Subscriber List

        # Make Arrary of appropriate 

        
        msg = EmailMessage(sender=self.request.get('sender'),
                           to=self.request.get('to'),
                           subject = self.request.get('subject'))
        
        if self.request.get('cc') != '':
            msg.initialize(cc=self.request.get('cc'))

        msg.send()

class MakeTab(webapp2.RequestHandler):
    def post(self): # should run at most 1/s
        
        msg = EmailMessage(sender=self.request.get('sender'),
                           to=self.request.get('to'),
                           subject = self.request.get('subject'))
        
        if self.request.get('cc') != '':
            msg.initialize(cc=self.request.get('cc'))

        msg.send()
