#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     11/07/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import jinja2
import os
import webapp2
import logging
import calendar
import urllib
import httplib
import shutil
from ..lib import xlwt
from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api import taskqueue

class SevenElevenReport(webapp2.RequestHandler):
    def post(self):
        urlfetch.set_default_fetch_deadline(100)
        date_start = "2013-06-23T00:00"
        date_end =   "2013-06-30T23:59"
        taskqueue.add(url='/sample_Email', params = {'Email':'rjain@kou.pn' ,'ReportName':'7-11 Report',
                    'Name': '7-Eleven' , 'ID': 15 , 'Start': date_start,
                    'End': date_end , 'Offer': True ,'weekday':6 })


