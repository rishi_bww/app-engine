#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     25/06/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import jinja2
import os
import webapp2
import logging
import datetime

from google.appengine.ext import ndb
from google.appengine.api import search
from google.appengine.api import users
from google.appengine.api.mail import EmailMessage
from ..models.ReportList import ReportList as List

from emailer import SendEmail


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)

class Reporter(webapp2.RedirectHandler):
    def get(self):
        list = List.query().order(-List.date_created)
        listreport = list.fetch(20)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja_environment.get_template('reporting.html')
        self.response.out.write(template.render(listreport=listreport,
                                                url=url,
                                                url_linktext=url_linktext))

class EditReport(webapp2.RedirectHandler):
    def get(self,names):
        list = List.query(List.reportName == str(names))
        listquery = list.fetch(10)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja_environment.get_template('editreport.html')
        self.response.out.write(template.render(names=names,listquery=listquery,
                                                url=url,
                                                url_linktext=url_linktext))

    def post(self,names):
        reporter = List()
        reporter.ClientId = self.request.get('ID')
        reporter.date_start = datetime.datetime.strptime(self.request.get('Start'),"%Y-%m-%dT%H:%M")
        reporter.date_end = datetime.datetime.strptime(self.request.get('End'),"%Y-%m-%dT%H:%M")
        if self.request.get('User'):
            reporter.user = True
        if self.request.get('Offer'):
            reporter.offer = True
        if self.request.get('Store'):
            reporter.store = True
        if self.request.get('Michaels'):
            reporter.michaels = True
        if self.request.get('Week'):
            reporter.frequency = "Weekly"
            reporter.weekly = True
            reporter.Scheduled = self.request.get('weekday')
        if self.request.get('Month'):
            reporter.frequency = "Monthly"
            reporter.monthly = True
            reporter.Scheduled = self.request.get('monthday')
        reporter.date_created = datetime.datetime.now()
        reporter.put()
        self.response.write(reporter)
        SendEmail()



