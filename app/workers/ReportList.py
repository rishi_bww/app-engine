#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     25/06/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from google.appengine.ext import ndb

class ReportList(ndb.Model):
    ClientId = ndb.StringProperty(indexed = False)
    reportName = ndb.StringProperty()
    frequency = ndb.StringProperty(indexed=False)
    Scheduled = ndb.StringProperty(indexed=False)
    date_created = ndb.DateTimeProperty(auto_now)