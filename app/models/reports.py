from google.appengine.ext import db

class Report(db.Model):
    ReportName = db.StringProperty(required=True)
    ScheduleType = db.StringProperty(required=True, choices=set(["weekly", "monthly"])) # time frame
    ScheduleValue = db.StringProperty(required=True) # Mon, Tues - OR - 1, 3, 25, 31
    ClientName = db.StringProperty(required=True)
    DateStart= db.StringProperty(required=True)
    DateStop= db.StringProperty(required=True)
    Permissions Array= db.JsonProperty(required=True)
	ListSubscribers = db.JsonProperty(required=True)
    # specify what custom logic to trigger


#IntegerProperty			64-bit signed integer
#FloatProperty				Double-precision floating-point number
#BooleanProperty			Boolean
#StringProperty				Unicode string; up to 500 characters, indexed
#TextProperty				Unicode string; unlimited length, not indexed
#BlobProperty				Uninterpreted byte string if you set indexed=True, up to 500 characters, indexed; if indexed is False (the default), unlimited length, not indexed. Optional keyword argument: compressed.
#DateTimeProperty			Date and time (see Date and Time Properties)
#DateProperty				Date (see Date and Time Properties)
#TimeProperty				Time (see Date and Time Properties)
#GeoPtProperty				Geographical location. This is a ndb.GeoPt object. The object has attributes lat and lon, both floats. You can construct one with two floats like ndb.GeoPt(52.37, 4.88) or with a string ndb.GeoPt("52.37, 4.88"). (This is actually the same class as db.GeoPt)
#KeyProperty				Datastore key Optional keyword argument: kind=kind, to require that keys assigned to this property always have the indicated kind. May be a string or a Model subclass.
#BlobKeyProperty			Blobstore key Corresponds to BlobReferenceProperty in the old db API, but the property value is a BlobKey instead of a BlobInfo; you can construct a BlobInfo from it using BlobInfo(blobkey)
#UserProperty				User object.
#StructuredProperty			Includes one kind of model inside another, by value (see Structured Properties)
#LocalStructuredProperty	Like StructuredProperty, but on-disk representation is an opaque blob and is not indexed (see Structured Properties). Optional keyword argument: compressed.
#JsonProperty				Value is a Python object (such as a list or a dict or a string) that is serializable using Python's json module; the Datastore stores the JSON serialization as a blob. Unindexed by default. Optional keyword argument: compressed.
#PickleProperty				Value is a Python object (such as a list or a dict or a string) that is serializable using Python's pickle protocol; the Datastore stores the pickle serialization as a blob. Unindexed by default. Optional keyword argument: compressed.
#GenericProperty			Generic value Used mostly by the Expando class, but also usable explicitly. Its type may be any of int, long, float, bool, str, unicode, datetime, Key, BlobKey, GeoPt, User, None.
#ComputedProperty			Value computed from other properties by a user-defined function. (See Computed Properties.#)