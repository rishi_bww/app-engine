# Koupon Media on App Engine

OVERVIEW
----------
Comments are ALMOST more IMPORTANT than the code.
Life is easier when people understand what you are trying to accomplish
I am most easily able to assess your work based on your ability to communicate through comments
I can not express or over-empahsize their importance to our success as a team


Convention
----------
Contollers DO STUFF. These files map to a group of functional needs that often pull a number of similar objects together to 
accomplish a task
Views SHOW STUFF. A view file (with parameters) is often the output of a controller. It is usually with the user sees. i.e. a visual representation of the data a user expects to see
Models ARE STUFF. Model files will be a 1:1 mapping to object defintions typically used via controllers


What Going On?
--------------
GAE starts with the app.yaml to figure out what to do with your app. We have defined it to look for this file and the routes variable specifically inside of this file. The routes variable uses "lazy loading" of modules to map the URLS we care about to functions that live inside of our files - that live inside of the defined folders in this repo.


## Products
- [App Engine][1]

## Language
- [Python][2]

## APIs
- [NDB Datastore API][3]
- [Users API][4]

## Dependencies
- [webapp2][5]
- [jinja2][6]
- [Twitter Bootstrap][7]

[1]: https://developers.google.com/appengine
[2]: https://python.org
[3]: https://developers.google.com/appengine/docs/python/ndb/
[4]: https://developers.google.com/appengine/docs/python/users/
[5]: http://webapp-improved.appspot.com/
[6]: http://jinja.pocoo.org/docs/
[7]: http://twitter.github.com/bootstrap/